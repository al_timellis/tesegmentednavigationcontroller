//
//  MainPushedViewController.m
//  TESegmentedNavigationControllerExample
//
//  Created by Timothy Ellis on 8/06/2016.
//  Copyright © 2016 Tim Ellis. All rights reserved.
//

#import "MainPushedViewController.h"
#import "TESegmentedNavigationController.h"

@interface MainPushedViewController ()

@end

@implementation MainPushedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

	self.rightButtons = @[
						  [[UIBarButtonItem alloc] initWithTitle:@"PushedR" style:UIBarButtonItemStyleDone target:self action:nil]
						  ];
	self.navigationFooter = @"Pushed footer text here";
}

@end
