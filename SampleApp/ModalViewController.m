//
//  ModalViewController.m
//  TESegmentedNavigationControllerExample
//
//  Created by Timothy Ellis on 3/06/2016.
//  Copyright © 2016 Tim Ellis. All rights reserved.
//

#import "ModalViewController.h"
#import "TESegmentedNavigationController.h"

@interface ModalViewController () <TESegmentedNavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet UILabel *label;

@end

@implementation ModalViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	
	TESegmentedNavigationController *navController = (TESegmentedNavigationController *)self.navigationController;
	
	TESegmentedControl *control = navController.segmentedControl;
	[control appendSegments:@[
							  [TESegment segmentWithTitle:@"Segment 1"],
							  [TESegment segmentWithTitle:@"Segment 2"],
							  [TESegment segmentWithTitle:@"Segment 3"],
							  ] animated:NO];
	
	self.title = @"Modal View Controller";
	
	//Arbitrary button names testing short medium and longer length buttons
	self.rightButtons = @[
								   [UIButton buttonWithTitle:@"Test 1" target:self action:@selector(navButtonTouchUp:)],
								   [UIButton buttonWithTitle:@"R2" target:self action:@selector(navButtonTouchUp:)],
								   [UIButton buttonWithTitle:@"Testing 3" target:self action:@selector(navButtonTouchUp:)],
								   ];
	
	navController.delegateSeg = self;
	[navController.navBar setCancelButtonImage:[UIImage imageNamed:@"nav_close"]];
	[navController.navBar setCancelButtonTitle:@""];
}

- (void)navButtonTouchUp:(UIButton *)sender {
	self.label.text = [NSString stringWithFormat:@"Tapped navigation button with title: %@", sender.currentTitle];
}

- (void)onNavigationController:(TESegmentedNavigationController *)navigationController tabChanged:(TESegmentedControl *)sender {
	self.label.text = [NSString stringWithFormat:@"Tapped Idx: %ld\nSelected Idx: %ld", (long)sender.lastTappedIndex, (long)sender.selectedIndex];
}

@end
