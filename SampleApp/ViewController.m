//
//  ViewController.m
//  SampleApp
//
//  Created by Timothy Ellis on 16/05/2016.
//  Copyright © 2016 Tim Ellis. All rights reserved.
//

#import "ViewController.h"
#import "TESegmentedNavigationController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
	self.segmentedNavigationController.navBarBackground = [UIColor clearColor];
	
	self.title = @"My Great App";
	
	self.rightButtons = @[
						  [[UIBarButtonItem alloc] initWithTitle:@"Change Color" style:UIBarButtonItemStyleDone target:self action:@selector(changeColorAction:)]
						  ];
	__weak typeof(self) _weak_self = self;
	self.navigationTitleTapAction = ^{
		if (!_weak_self) {
			NSLog(@"WARNING: Weak self is nil");
			return;
		}
		UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"navigationTitleTapAction"
																	   message:[NSString stringWithFormat:@"<%@ %p> Executing block located: %s:%d", _weak_self.class, _weak_self, __FILE__, __LINE__]
																preferredStyle:UIAlertControllerStyleAlert
									];
		[alert addAction:[UIAlertAction actionWithTitle:@"Ok"
												  style:UIAlertActionStyleDefault
												handler:nil
							]
		 ];
		[_weak_self presentViewController:alert animated:YES completion:nil];
		static int count = 1;
		_weak_self.segmentedNavigationController.navBar.footer = [NSString stringWithFormat:@"%@ %d", _weak_self.segmentedNavigationController.navBar.footer, count++];
	};
	[self.segmentedNavigationController.navBar setBackButtonTitle:@""];
	self.navigationFooter = @"The footer text";
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

- (void)changeColorAction:(id)sender {
	static NSArray *colors = nil;
	if (!colors) {
		colors = @[
				   [UIColor orangeColor],
				   [UIColor redColor],
				   [UIColor blueColor],
				   [UIColor cyanColor],
				   [UIColor whiteColor],
				   [UIColor blackColor],
				   ];
	}
	self.segmentedNavigationController.navBarBackground = [colors objectAtIndex:(NSUInteger)arc4random_uniform((u_int32_t)colors.count)];
}

@end
