Pod::Spec.new do |s|
    s.name				=	'TESegmentedNavigationController'
    s.version			=	'0.19'
    s.license			=	{
        :type => 'MIT',
        :text => <<-LICENSE
            TESegmentedNavigationController
            
            The MIT License (MIT)
            Copyright (c) 2016 Tim Ellis
            
            Permission is hereby granted, free of charge, to any person obtaining a copy
            of this software and associated documentation files (the "Software"), to deal
            in the Software without restriction, including without limitation the rights
            to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
            copies of the Software, and to permit persons to whom the Software is
            furnished to do so, subject to the following conditions:
            
            The above copyright notice and this permission notice shall be included
            in all copies or substantial portions of the Software.
            
            THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
            IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
            FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
            AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
            LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
            OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
            THE SOFTWARE.
        LICENSE
    }
    s.summary			=	'Embeded glorified radio buttons into a navigation controller.'
    s.description		=	<<-DESC
    							A drop in replacement for UINavigationController, TESegmentedNavigationController allows 
                                you to add a pretty segemented bar below your navgation bar.
    						DESC

    s.homepage			=	'https://bitbucket.org/al_timellis/tesegmentednavigationcontroller'
    s.authors			=	{
        'Tim Ellis' => 'crazyivan444@gmail.com'
    }
    s.source			=	{
        :git => 'https://bitbucket.org/al_timellis/tesegmentednavigationcontroller.git',
        :tag => '0.19'
    }
    
    s.ios.deployment_target = "8.2"
    
    s.source_files		=	[
		'TESegmentedNavigationController/**/*.{h,m}'
    ]
    s.resource_bundles  =   {
        'TESegmentedNavigationController'   =>  'TESegmentedNavigationController/Resources/*.png'
    }
    s.requires_arc		=	true
    
    s.dependency "TESegmentedControl", "~> 0.2"
end
