//
//  UIBarButtonItem+TESegmentedNavigationController.m
//  TESegmentedNavigationController
//
//  Created by Timothy Ellis on 8/06/2016.
//  Copyright © 2016 Tim Ellis. All rights reserved.
//

#import "UIBarButtonItem+TESegmentedNavigationController.h"
#import "UIButton+TESegmentedNavigationController.h"

@implementation UIBarButtonItem (TESegmentedNavigationController)

- (UIButton *)toUIButton {
	return [UIButton buttonWithTitle:self.title image:self.image target:self.target action:self.action];
}

@end
