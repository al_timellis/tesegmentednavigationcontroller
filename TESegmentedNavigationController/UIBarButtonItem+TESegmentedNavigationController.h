//
//  UIBarButtonItem+TESegmentedNavigationController.h
//  TESegmentedNavigationController
//
//  Created by Timothy Ellis on 8/06/2016.
//  Copyright © 2016 Tim Ellis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIBarButtonItem (TESegmentedNavigationController)

- (nonnull UIButton *)toUIButton;

@end
