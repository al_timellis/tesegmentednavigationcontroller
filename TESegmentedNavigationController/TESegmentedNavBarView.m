//
//  TESegmentedNavBarView.m
//  TESegmentedNavigationController
//
//  The MIT License (MIT)
//  Copyright (c) 2016 Tim Ellis
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included
//  in all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import "TESegmentedNavBarView.h"
#import "TESegmentedNavigationController.h"
#import "UIBarButtonItem+TESegmentedNavigationController.h"
#import "NSAttributedString+TESegmentedNavigationController.h"
#import "UIImage+TESegmentedNavigationController.h"
#import "TEButtonFooter.h"

@interface TESegmentedNavBarView ()

@property (nonatomic, nonnull) TEButtonFooter *titleButton;
@property (nonatomic, nonnull) UIButton *backButton;
@property (nonatomic, nonnull) UIButton *cancelButton;

@property (nonatomic) NSLayoutConstraint *constraintSegmentedControlHeight;

/// Contains the constraints for the title label and buttons
@property (nonatomic, nonnull) NSArray <NSLayoutConstraint *>* constraintsLabelAndButtons;

@property (nonatomic, nonnull) UIView *statusBarBackView;

@end

static const NSTimeInterval TESegmentedNavBarAnimationDuration = 0.1;
static const CGFloat TESegmentedNavBarViewHeightPortrait = 39.6f;
static const CGFloat TESegmentedNavBarViewHeightLandscape = 28.8f;
static const CGFloat TESegmentedNavBarViewHeightSegControlPortrait = 40.0f;
static const CGFloat TESegmentedNavBarViewHeightSegControlLandscape = 32.0f;
static const CGFloat TESegmentedNavBarViewTitleButtonHeight = 27.0f;

static const CGFloat TESegmentedNavBarViewButtonPadding = 4.0f;
static const CGFloat TESegmentedNavBarViewButtonMinWidth = 24.0f;

@implementation TESegmentedNavBarView

@synthesize translucent = _translucent;

#pragma mark - Sizing methods
// Overriden to support resizing
- (CGSize)sizeThatFits:(CGSize)size {
	CGSize actualSize = [super sizeThatFits:size];
	actualSize.width = [self getRootWindowWidth];
	actualSize.height = [self getHeight] + [self getTabHeight];
	actualSize.width = floor(actualSize.width);
	actualSize.height = floor(actualSize.height);
	return actualSize;
}

- (CGFloat)getTabHeight {
	if (!self.tabShown) {
		return 0.0f;
	}
	if (UIDeviceOrientationIsLandscape([[UIDevice currentDevice] orientation])) {
		return TESegmentedNavBarViewHeightSegControlLandscape;
	}
	return TESegmentedNavBarViewHeightSegControlPortrait;
}

- (CGFloat)getHeight {
	if (UIDeviceOrientationIsLandscape([[UIDevice currentDevice] orientation])) {
		return TESegmentedNavBarViewHeightLandscape;
	}
	return TESegmentedNavBarViewHeightPortrait;
}

- (CGFloat)getRootWindowWidth {
	UIWindow *window = [[UIApplication sharedApplication] keyWindow];
	if (window == nil) {
		window = [[[UIApplication sharedApplication] windows] firstObject];
		if (window == nil) {
			return self.bounds.size.width;
		}
	}
	return window.bounds.size.width;
}

#pragma mark - Getters/Setters

- (void)setTranslucent:(BOOL)translucent {
	_translucent = translucent;
	self.statusBarBackView.backgroundColor = [UIColor colorWithWhite:(translucent ? (250.0/255.0) : 1.0) alpha:(translucent ? 0.8 : 1.0)];
}

- (void)setTitleTextAlignment:(NSTextAlignment)titleTextAlignment {
	_titleTextAlignment = titleTextAlignment;
	static NSDictionary *map;
	if (!map) {
		map = @{
				@(NSTextAlignmentLeft): @(UIControlContentHorizontalAlignmentRight),
				@(NSTextAlignmentCenter): @(UIControlContentHorizontalAlignmentCenter),
				@(NSTextAlignmentRight): @(UIControlContentHorizontalAlignmentLeft),
				@(NSTextAlignmentJustified): @(UIControlContentHorizontalAlignmentFill),
				@(NSTextAlignmentNatural): @(UIControlContentHorizontalAlignmentCenter),
				};
	}
	self.titleButton.contentHorizontalAlignment = [(map[@(titleTextAlignment)] ?: @(UIControlContentHorizontalAlignmentCenter)) integerValue];
	self.titleButton.footerLabel.textAlignment = titleTextAlignment;
}

- (void)setTitleTapAction:(void (^)(void))titleTapAction {
	_titleTapAction = titleTapAction;
	self.titleButton.enabled = (titleTapAction != nil);
	[self updateTitleLabel];
}

- (void)setBackButtonTitle:(NSString *)title {
	[self.backButton setTitle:(title) ? title : NSLocalizedString(@"Back", @"Text shown in the back button") forState:UIControlStateNormal];
	[self.backButton sizeToFit];
	[self layoutTitleNav];
}

- (void)setCancelButtonTitle:(NSString *)title {
	[self.cancelButton setTitle:(title) ? title : NSLocalizedString(@"Cancel", @"Text shown in the cancel button") forState:UIControlStateNormal];
	[self.cancelButton sizeToFit];
	[self layoutTitleNav];
}

- (void)setCancelButtonImage:(UIImage *)image {
	[self.cancelButton setImage:image forState:UIControlStateNormal];
	[self.cancelButton sizeToFit];
	[self layoutTitleNav];
}

- (void)setTitleAttributes:(NSDictionary<NSString *,id> *)titleAttributes {
	_titleAttributes = titleAttributes;
	[self updateTitleLabel];
}

- (void)setTitle:(NSString *)title {
	_title = title;
	[self updateTitleLabel];
}

- (void)setFooter:(NSString *)footer {
	_footer = footer;
	[self updateTitleLabel];
}

- (NSArray<UIButton *> *)convertBarButtonArrayToButtonWithArray:(NSArray<UIBarButtonItem *> *)array {
	if (!array || [array count] == 0) {
		return nil;
	}
	// First get the first element from the array and test it's class. If it's of kind UIBarButtonItem then proceed
	if ([[array firstObject] isKindOfClass:[UIBarButtonItem class]]) {
		NSMutableArray <UIButton *>*newButtonArray = [NSMutableArray new];
		for (UIBarButtonItem *button in array) {
			[newButtonArray addObject:[button toUIButton]];
		}
		return newButtonArray;
	}
	//  Otherwise test if it's of class kind UIButton.
	if ([[array firstObject] isKindOfClass:[UIButton class]]) {
		return (NSArray<UIButton *> *)array;
	}
	return nil;
}

- (void)setLeftButtons:(NSArray<UIButton *> *)leftButtons {
	if (self.constraintsLabelAndButtons) {
		[NSLayoutConstraint deactivateConstraints:self.constraintsLabelAndButtons];
	}
	for (UIButton *button in _leftButtons) {
		[button removeFromSuperview];
	}
	leftButtons = [self convertBarButtonArrayToButtonWithArray:(NSArray<UIBarButtonItem *> *)leftButtons];
	_leftButtons = (leftButtons) ? leftButtons : [leftButtons.class new];
	[self layoutTitleNav];
}

- (void)setRightButtons:(NSArray<UIButton *> *)rightButtons {
	if (self.constraintsLabelAndButtons) {
		[NSLayoutConstraint deactivateConstraints:self.constraintsLabelAndButtons];
	}
	for (UIButton *button in _rightButtons) {
		[button removeFromSuperview];
	}
	rightButtons = [self convertBarButtonArrayToButtonWithArray:(NSArray<UIBarButtonItem *> *)rightButtons];
	_rightButtons = (rightButtons) ? rightButtons : [rightButtons.class new];
	[self layoutTitleNav];
}

- (void)setTabShown:(BOOL)tabShown animated:(BOOL)animated {
	_tabShown = tabShown;
	self.segmentedControl.alpha = tabShown;
	self.segmentedControl.hidden = !tabShown;
	[UIView animateWithDuration:animated ? TESegmentedNavBarAnimationDuration : 0 animations:^{
		self.constraintSegmentedControlHeight.constant = tabShown ? [self getTabHeight] : 0;
	}];
	[self resizeAnimated:animated];
}

- (void)setTabShown:(BOOL)tabShown {
	[self setTabShown:tabShown animated:YES];
}

#pragma mark - Custom Methods

- (nullable UIImage *)imageNamedFromPodBundle:(NSString *)imageNamed {
	UIImage *image = [UIImage imageNamed:imageNamed];
	if (!image) {
		//When loaded from a pod, Cocoapods will generate a bundle for the images
		image = [UIImage imageNamed:[NSString stringWithFormat:@"TESegmentedNavigationController.bundle/%@", imageNamed]];
	}
	return image;
}

- (void)updateTitleLabel {
	NSString *titleStr = (self.title == nil) ? @"" : self.title;

	self.titleButton.updatesEnabled = NO;
	self.titleButton.title = titleStr;
	self.titleButton.footer = self.footer;
	
	UIImage *forewordChevron = [self imageNamedFromPodBundle:@"TESegmentedNavigationController-Foreword-Chevron"];
	UIImage *emptyImage = [UIImage emptyImageWithSize:forewordChevron.size];
	self.titleButton.titleImage = self.titleButton.enabled ? forewordChevron : emptyImage;
	self.titleButton.updatesEnabled = YES;
	
}

- (nullable TESegmentedNavigationController *)findParent {
	UIResponder *currentResponder = [self nextResponder];
	while (currentResponder != nil) {
		if ([currentResponder isKindOfClass:[TESegmentedNavigationController class]]) {
			return (TESegmentedNavigationController *)currentResponder;
		}
		currentResponder = [currentResponder nextResponder];
	}
	return nil;
}

- (void)appendButtons:(nonnull NSArray *)buttons toVisualFormat:(NSMutableString * _Nonnull *)visualFormat withButtonStrPrefix:(nonnull NSString *)buttonPrefix andViewsDictionary:(NSMutableDictionary * _Nonnull *)viewsDictionary {
	NSInteger buttonIdx;
	buttonIdx = 0;
	for (UIButton *button in buttons) {
		if (button.superview) {
			if (button.superview != self) {
				[button removeFromSuperview];
				[self addSubview:button];
			}
		}
		else {
			[self addSubview:button];
		}
		button.hidden = NO;
		NSString *buttonStr = [NSString stringWithFormat:@"%@%ld", buttonPrefix, (long)buttonIdx];
		CGFloat buttonWidth = floorf(MAX(button.bounds.size.width, TESegmentedNavBarViewButtonMinWidth));
		[*visualFormat appendFormat:@"-[%@(%f)]", buttonStr, buttonWidth];
		[*viewsDictionary setObject:button forKey:buttonStr];
		buttonIdx++;
	}
}

- (void)layoutTitleNav {
	if (self.constraintsLabelAndButtons) {
		[NSLayoutConstraint deactivateConstraints:self.constraintsLabelAndButtons];
	}
	/// The visual format string used to lay out the constraints
	NSMutableString *visualFormat = [NSMutableString stringWithString:@"H:|"];
	/// The dictionary of string-view mapping used with laying out the constraints.
	NSMutableDictionary *viewsDict = [NSMutableDictionary new];
	
	if (self.parent) {
		if ([self.parent.viewControllers count] > 1 && self.showBackButton) {
			// More than 1 view controller on the stack. Show the back button
			self.backButton.hidden = NO;
			self.cancelButton.hidden = YES;
			CGFloat buttonWidth = floorf(MAX(self.backButton.bounds.size.width, TESegmentedNavBarViewButtonMinWidth));
			[visualFormat appendFormat:@"-[backButton(%f)]", buttonWidth];
			[viewsDict setObject:self.backButton forKey:@"backButton"];
		}
		else {
			// Hide the back button
			self.backButton.hidden = YES;
			if ([self.parent.viewControllers count] <= 1 && [self.parent isModal]) {
				self.cancelButton.hidden = NO;
				CGFloat buttonWidth = floorf(MAX(self.cancelButton.bounds.size.width, TESegmentedNavBarViewButtonMinWidth));
				[visualFormat appendFormat:@"-[cancelButton(%f)]", buttonWidth];
				[viewsDict setObject:self.cancelButton forKey:@"cancelButton"];
			}
		}
	}
	
	[self appendButtons:self.leftButtons
		 toVisualFormat:&visualFormat
	withButtonStrPrefix:@"leftButton"
	 andViewsDictionary:&viewsDict
	 ];
	
	[visualFormat appendString:@"-[titleLabel]"];
	[viewsDict setObject:self.titleButton forKey:@"titleLabel"];
	
	[self appendButtons:self.rightButtons
		 toVisualFormat:&visualFormat
	withButtonStrPrefix:@"rightButton"
	 andViewsDictionary:&viewsDict
	 ];

	// Close off the format saying we want to snap to the right edge
	if (![[visualFormat substringFromIndex:visualFormat.length - 1] isEqualToString:@"-"]) {
		[visualFormat appendString:@"-"];
	}
	[visualFormat appendString:@"|"];
	
	self.constraintsLabelAndButtons = [NSLayoutConstraint constraintsWithVisualFormat:visualFormat
																			  options:0
																			  metrics:nil
																				views:viewsDict
									   ];
	
	NSMutableArray *allButtons = [NSMutableArray new];
	NSMutableArray *allButtonsYConstraints = [NSMutableArray new];
	if (self.showBackButton) {
		[allButtons addObject:self.backButton];
	}
	if (self.backButton.hidden) {
		[allButtons addObject:self.cancelButton];
	}
	[allButtons addObjectsFromArray:self.leftButtons];
	[allButtons addObjectsFromArray:self.rightButtons];
	CGFloat buttonHeight = [self getHeight] - TESegmentedNavBarViewButtonPadding;
	for (UIButton *button in allButtons) {
		[allButtonsYConstraints addObject:[NSLayoutConstraint constraintWithItem:button
																	   attribute:NSLayoutAttributeCenterY
																	   relatedBy:NSLayoutRelationEqual
																		  toItem:self.titleButton
																	   attribute:NSLayoutAttributeCenterY
																	  multiplier:1.0
																		constant:0.0
										   ]
		 ];
		
		[allButtonsYConstraints addObject:[NSLayoutConstraint constraintWithItem:button
																	   attribute:NSLayoutAttributeHeight
																	   relatedBy:NSLayoutRelationGreaterThanOrEqual
																		  toItem:nil
																	   attribute:NSLayoutAttributeNotAnAttribute
																	  multiplier:1.0
																		constant:buttonHeight
										   ]
		 ];
		
	}
	self.constraintsLabelAndButtons = [self.constraintsLabelAndButtons arrayByAddingObjectsFromArray:allButtonsYConstraints];
	
	[NSLayoutConstraint activateConstraints:self.constraintsLabelAndButtons];
}

- (void)resizeAnimated:(BOOL)animated {
	CGRect newFrame = self.frame;
	newFrame.size = [self sizeThatFits:CGSizeZero];
	
	CGFloat newFrameY = newFrame.origin.y;
	if (newFrameY > 0) {
		CGRect statusBarRect = CGRectZero;
		statusBarRect.origin.y = -newFrameY;
		statusBarRect.size.width = newFrame.size.width;
		statusBarRect.size.height = newFrameY;
		[self.statusBarBackView setFrame:statusBarRect];
	}
	self.statusBarBackView.hidden = newFrameY <= 0;
	
	[UIView animateWithDuration:(animated ? TESegmentedNavBarAnimationDuration : 0.0) animations:^{
		[self setFrame:newFrame];
	}];
}

- (void)setupConstraints {
	[self layoutTitleNav];
	NSArray *hConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[obj]|"
																	options:0
																	metrics:0
																	  views:@{
																			  @"obj":self.segmentedControl
																			  }
							 ];
	[NSLayoutConstraint activateConstraints:hConstraints];

	NSLayoutConstraint *yPosConstraint = [NSLayoutConstraint constraintWithItem:self.segmentedControl
																	  attribute:NSLayoutAttributeBottom
																	  relatedBy:NSLayoutRelationEqual
																		 toItem:self
																	  attribute:NSLayoutAttributeBottom
																	 multiplier:1.0f
																	   constant:0.0f
										  ];
	yPosConstraint.active = YES;
	
	self.constraintSegmentedControlHeight = [NSLayoutConstraint constraintWithItem:self.segmentedControl
																		 attribute:NSLayoutAttributeHeight
																		 relatedBy:NSLayoutRelationEqual
																			toItem:nil
																		 attribute:NSLayoutAttributeNotAnAttribute
																		multiplier:1.0f
																		  constant:TESegmentedNavBarViewHeightSegControlPortrait
											 ];
	self.constraintSegmentedControlHeight.active = YES;
	
	NSArray *titleTopConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-[title(titleHeight)]"
																		   options:0
																		   metrics:@{
																					 @"titleHeight": @(TESegmentedNavBarViewTitleButtonHeight)
																					 }
																			 views:@{
																					 @"title": self.titleButton,
																					 }
									];
	[NSLayoutConstraint activateConstraints:titleTopConstraints];
}

- (void)setupViewWithParent:(nullable TESegmentedNavigationController *)parent {
	if (self.segmentedControl && self.titleButton) {
		return;
	}
	self.parent = (parent) ? parent : [self findParent];
	
	// Hide the super class's title text attributes
	[self setTitleTextAttributes:@{
								   NSForegroundColorAttributeName: [UIColor clearColor],
								   NSBackgroundColorAttributeName: [UIColor clearColor],
								   }];
	self.statusBarBackView = [UIView new];
	self.statusBarBackView.translatesAutoresizingMaskIntoConstraints = NO;
	[self.statusBarBackView setBackgroundColor:[UIColor whiteColor]];
	self.statusBarBackView.hidden = YES;
	[self addSubview:self.statusBarBackView];
	
	self.segmentedControl = [TESegmentedControl new];
	self.segmentedControl.translatesAutoresizingMaskIntoConstraints = NO;
	[self addSubview:self.segmentedControl];
	
	self.titleButton = [TEButtonFooter buttonWithFooter:nil];
	[self.titleButton.titleLabel setFont:[UIFont preferredFontForTextStyle:UIFontTextStyleHeadline]];
	[self.titleButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
	
	[self.titleButton setTitleColor:[UIColor blackColor] forState:UIControlStateDisabled];
	[self.titleButton setEnabled:NO];
	[self.titleButton addTarget:self action:@selector(titleButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
	self.titleButton.translatesAutoresizingMaskIntoConstraints = NO;
	[self addSubview:self.titleButton];
	
	self.showBackButton = YES;
	
	UIImage *backButtonImage = [self imageNamedFromPodBundle:@"TESegmentedNavigationController-Back-Chevron"];
	
	self.backButton = [UIButton buttonWithTitle:@"Back"
										  image:backButtonImage
										 target:self
										 action:@selector(backButtonTapped:)
					   ];
	self.backButton.translatesAutoresizingMaskIntoConstraints = NO;
	[self.backButton setHidden:YES];
	[self addSubview:self.backButton];
	
	self.cancelButton = [UIButton buttonWithTitle:@"Cancel"
										   target:self
										   action:@selector(cancelButtonTapped:)
						 ];
	self.cancelButton.translatesAutoresizingMaskIntoConstraints = NO;
	[self.cancelButton setHidden:YES];
	[self addSubview:self.cancelButton];
	
	self.leftButtons = nil;
	self.rightButtons = nil;
	
	[self setupConstraints];
	self.translucent = NO;
	self.segmentedControl.opaque = NO;
	self.segmentedControl.backgroundColor = [UIColor clearColor];

	// To remove unnecessary navigation bar shadow that appears above segment items
	// on iOS 11
	if (@available(iOS 11.0, *)) {
		[self setShadowImage:[[UIImage alloc] init]];
	}
}

- (void)backButtonTapped:(id)sender {
	if (self.parent && self.parent.viewControllers && [self.parent.viewControllers count] > 1) {
		[self.parent popViewControllerAnimated:YES];
	}
}

- (void)cancelButtonTapped:(id)sender {
	[self.parent dismissViewControllerAnimated:YES completion:nil];
}

- (void)titleButtonTapped:(id)sender {
	if (self.titleTapAction != nil) {
		self.titleTapAction();
	}
}

#pragma mark - Init Methods

- (instancetype)initWithParent:(TESegmentedNavigationController *)parent {
	if (self = [super initWithFrame:CGRectZero]) {
		[self setupViewWithParent:parent];
	}
	return self;
}

#pragma mark - Overrides
- (void)setItems:(NSArray<UINavigationItem *> *)items {
	// Do not set the items
}

- (void)layoutSubviews {
	[super layoutSubviews];
	
	for (__kindof UIView *view in [self subviews]) {
		NSString *viewClassName = NSStringFromClass([view class]);
		if ([viewClassName containsString:@"UINavigationItemView"]) {
			view.alpha = 0.0f;
		}
	}

	[self layoutTitleNav];
	
	// Fix the corrupted navigation bar on iOS 11.
	// UINavigation controller does not allow to resize navigationBar on iOS 11.
	// This code puts down TESegmentedControl under the navigationBar manually.
	// The one thing it does not allow to increase clickable area on navigationBar.
	if (@available(iOS 11.0, *)) {
		for (UIView *subview in self.subviews) {
			NSString* subViewClassName = NSStringFromClass([subview class]);
			if ([subViewClassName containsString:@"TESegmentedControl"]) {
				CGRect subViewFrame = subview.frame;
				subViewFrame.origin.y = TESegmentedNavBarViewTitleButtonHeight;
				subViewFrame.size.height = [self getTabHeight];
				[subview setFrame: subViewFrame];
			}
		}
	}
}

- (void)drawRect:(CGRect)rect {
	// We override this to prevent UIBarBackground from being drawn
}

@end
