//
//  UIImage+TESegmentedNavigationController.h
//  TESegmentedNavigationControllerExample
//
//  Created by Timothy Ellis on 13/07/2016.
//  Copyright © 2016 Tim Ellis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (TESegmentedNavigationController)

/**
 *	Generates an empty image with a given size
 *
 *	@param size	The size of the empty image to generate
 *
 *	@return The empty image
 */
+ (UIImage *)emptyImageWithSize:(CGSize)size;

@end
