//
//  UIViewController+TESegmentedNavigationController.m
//  TESegmentedNavigationControllerExample
//
//  Created by Timothy Ellis on 8/06/2016.
//  Copyright © 2016 Tim Ellis. All rights reserved.
//

#import "UIViewController+TESegmentedNavigationController.h"
#import "TESegmentedNavigationController.h"
#import <objc/runtime.h>

static const void *TESegmentedNavigationControllerLeftButtonsAssocKey = &TESegmentedNavigationControllerLeftButtonsAssocKey;
static const void *TESegmentedNavigationControllerRightButtonsAssocKey = &TESegmentedNavigationControllerRightButtonsAssocKey;
static const void *TESegmentedNavigationControllerTitleActionAssocKey = &TESegmentedNavigationControllerTitleActionAssocKey;
static const void *TESegmentedNavigationControllerNavigationFooterKey = &TESegmentedNavigationControllerNavigationFooterKey;

@interface TESegmentedNavigationController ()

// Expose the private properties
@property (nonatomic, nullable, weak) NSArray *vc_leftButtons;
@property (nonatomic, nullable, weak) NSArray *vc_rightButtons;
@property (nonatomic, nullable, weak) UIViewController *vc_current;

@end

@implementation UIViewController (TESegmentedNavigationController)

- (NSArray *)leftButtons {
	return objc_getAssociatedObject(self, TESegmentedNavigationControllerLeftButtonsAssocKey);
}

- (void)setLeftButtons:(NSArray *)leftButtons {
	objc_setAssociatedObject(self, TESegmentedNavigationControllerLeftButtonsAssocKey, leftButtons, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
	if (self.segmentedNavigationController) {
		self.segmentedNavigationController.vc_current = self;
		self.segmentedNavigationController.vc_leftButtons = leftButtons;
	}
}

- (NSArray *)rightButtons {
	return objc_getAssociatedObject(self, TESegmentedNavigationControllerRightButtonsAssocKey);
}

- (void)setRightButtons:(NSArray *)rightButtons {
	objc_setAssociatedObject(self, TESegmentedNavigationControllerRightButtonsAssocKey, rightButtons, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
	if (self.segmentedNavigationController) {
		self.segmentedNavigationController.vc_current = self;
		self.segmentedNavigationController.vc_rightButtons = rightButtons;
	}
}

- (void (^)(void))navigationTitleTapAction {
	return objc_getAssociatedObject(self, TESegmentedNavigationControllerTitleActionAssocKey);
}

- (void)setNavigationTitleTapAction:(void (^)(void))navigationTitleTapAction {
	objc_setAssociatedObject(self, TESegmentedNavigationControllerTitleActionAssocKey, navigationTitleTapAction, OBJC_ASSOCIATION_COPY_NONATOMIC);
	if (self.segmentedNavigationController) {
		self.segmentedNavigationController.navBar.titleTapAction = navigationTitleTapAction;
	}
}

- (NSString *)navigationFooter {
	return objc_getAssociatedObject(self, TESegmentedNavigationControllerNavigationFooterKey);
}

- (void)setNavigationFooter:(NSString *)navigationFooter {
	objc_setAssociatedObject(self, TESegmentedNavigationControllerNavigationFooterKey, navigationFooter, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
	if (self.segmentedNavigationController) {
		self.segmentedNavigationController.navBar.footer = navigationFooter;
	}
}

- (TESegmentedNavigationController *)segmentedNavigationController {
	if ([self.navigationController isKindOfClass:[TESegmentedNavigationController class]]) {
		return (TESegmentedNavigationController *)self.navigationController;
	}
	return nil;
}

@end
