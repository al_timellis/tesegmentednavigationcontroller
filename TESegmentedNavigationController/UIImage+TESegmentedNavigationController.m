//
//  UIImage+TESegmentedNavigationController.m
//  TESegmentedNavigationControllerExample
//
//  Created by Timothy Ellis on 13/07/2016.
//  Copyright © 2016 Tim Ellis. All rights reserved.
//

#import "UIImage+TESegmentedNavigationController.h"

@implementation UIImage (TESegmentedNavigationController)

+ (UIImage *)emptyImageWithSize:(CGSize)size {
	UIGraphicsBeginImageContextWithOptions(size, NO, 0);
	[[UIColor clearColor] setFill];
	UIRectFill(CGRectMake(0, 0, size.width, size.height));
	UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	return image;
}

@end
