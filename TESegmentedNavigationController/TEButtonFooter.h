//
//  TEButtonFooter.h
//  ButtonFooter
//
//  Created by Timothy Ellis on 11/07/2016.
//  Copyright © 2016 Tim Ellis. All rights reserved.
//

#import <UIKit/UIKit.h>

IB_DESIGNABLE
@interface TEButtonFooter : UIButton

/// The label below the title. Use the `footer` attribute for updating the text
@property (nonatomic, nonnull, readonly) UILabel *footerLabel;

/// Should updates be shown immediately?
@property (nonatomic) BOOL updatesEnabled;

/// The title text of the button
@property (nonatomic, nullable) IBInspectable NSString *title;

/// The text shown below the title text
@property (nonatomic, nullable) IBInspectable NSString *footer;

/// Any additional attributes to apply to the footer
@property (nonatomic, nullable) NSDictionary *footerAttributes;

/// The image to be displayed next to the title text (will be shown on the right hand side)
@property (nonatomic, nullable) IBInspectable UIImage *titleImage;

/**
 *	The initialiser that should be used to create an instance
 *
 *	@param footer	The footer text (nullable)
 */
+ (nonnull instancetype)buttonWithFooter:(nullable NSString *)footer;

/**
 *	Forcefully updates the display
 */
- (void)updateFields;

@end
