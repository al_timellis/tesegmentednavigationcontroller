//
//  UIViewController+TESegmentedNavigationController.h
//  TESegmentedNavigationControllerExample
//
//  Created by Timothy Ellis on 8/06/2016.
//  Copyright © 2016 Tim Ellis. All rights reserved.
//

#import <UIKit/UIKit.h>
@class TESegmentedNavigationController;

@interface UIViewController (TESegmentedNavigationController)

@property (nonatomic, nullable) NSArray *leftButtons;
@property (nonatomic, nullable) NSArray *rightButtons;
@property (nonatomic, copy, nullable) void (^navigationTitleTapAction)(void);
@property (nonatomic, nullable) NSString *navigationFooter;

@property (nonatomic, nullable, readonly) TESegmentedNavigationController *segmentedNavigationController;

@end
