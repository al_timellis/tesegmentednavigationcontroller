//
//  TESegmentedNavBarView.h
//  TESegmentedNavigationController
//
//  The MIT License (MIT)
//  Copyright (c) 2016 Tim Ellis
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy
//  of this software and associated documentation files (the "Software"), to deal
//  in the Software without restriction, including without limitation the rights
//  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
//  copies of the Software, and to permit persons to whom the Software is
//  furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included
//  in all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
//  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
//  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
//  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
//  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
//  THE SOFTWARE.
//

#import <UIKit/UIKit.h>
#import "UIButton+TESegmentedNavigationController.h"

static const CGFloat TESegmentedNavBarViewHeightSegControlPortrait;
static const CGFloat TESegmentedNavBarViewHeightSegControlLandscape;

@class TESegmentedNavigationController;
@class TESegmentedControl;

/**
 *	Custom Naviation Bar shown in a TESegmentedNavigationController.
 */
@interface TESegmentedNavBarView : UINavigationBar

/// `true` will show the back button when the parent has more than 1 view controller on the stack
@property (nonatomic) BOOL showBackButton;

/**
 *	The navigation bar's parent
 *
 *  NB: I am aware that this breaks all MVC principles (i.e. a view knowing about its parent) but it makes for the most modular code.
 */
@property (nonatomic, nullable) TESegmentedNavigationController *parent;

/// `true` will show the segmented tab bar.
@property (nonatomic) BOOL tabShown;

/// The segmented control
@property (nonatomic, nonnull) TESegmentedControl *segmentedControl;

/// The text shown in the back button. Setting null uses the default title;
- (void)setBackButtonTitle:(nullable NSString *)title;

/// The text shown in the cancel button. Setting null uses the default title;
- (void)setCancelButtonTitle:(nullable NSString *)title;

/// The image shown in the cancel button
- (void)setCancelButtonImage:(nullable UIImage *)image;

/// The text shown in the title in the navigation bar
@property (nonatomic, nullable) NSString *title;

/// The text shown below the title in the navigation bar
@property (nonatomic, nullable) NSString *footer;

/// The attributes of the title text
@property (nonatomic, nullable) NSDictionary<NSString *,id> *titleAttributes;

/// The alignment of the title text
@property (nonatomic) NSTextAlignment titleTextAlignment;

/// The buttons to be shown to the left of the title. Can be an array with all UIButton or UIBarButtonItem
@property (nonatomic, nullable) NSArray *leftButtons;

/// The buttons to be shown to the right of the title. Can be an array with all UIButton or UIBarButtonItem
@property (nonatomic, nullable) NSArray *rightButtons;

/// The block of code to execute when the title text is tapped
@property (nonatomic, copy, nullable) void (^titleTapAction)(void);

/**
 *	Recalculate the size and resize it appropriately with optional animation
 *
 *	@param animated	`true` it animates the reisze. Otherwise it resizes with no animation
 */
- (void)resizeAnimated:(BOOL)animated;

// Disables access to the default init methods
/*! @abstract Use initWithParent: to init with a parent. */
- (nonnull instancetype)init NS_UNAVAILABLE;
/*! @abstract Use initWithParent: to init with a parent. */
- (nonnull instancetype)initWithFrame:(CGRect)frame NS_UNAVAILABLE;
/*! @abstract Use initWithParent: to init with a parent. */
- (nullable instancetype)initWithCoder:(nullable NSCoder *)coder NS_UNAVAILABLE;

/// Designated init method to set up the instance
- (nullable instancetype)initWithParent:(nullable TESegmentedNavigationController *)parent;

@end
