//
//  UIButton+TESegmentedNavigationController.h
//  TESegmentedNavigationController
//
//  Created by Timothy Ellis on 6/06/2016.
//  Copyright © 2016 Tim Ellis. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (TESegmentedNavigationController)

+ (nonnull instancetype)buttonWithTitle:(nullable NSString *)title
								  image:(nullable UIImage *)image
								 target:(nullable id)target
								 action:(nullable SEL)action;

+ (nonnull instancetype)buttonWithTitle:(nullable NSString *)title
								 target:(nullable id)target
								 action:(nullable SEL)action;

+ (nonnull instancetype)buttonWithImage:(nullable UIImage *)image
								 target:(nullable id)target
								 action:(nullable SEL)action;

- (void)setHeight:(CGFloat)height;

@end
